const path = require('path');
const express = require('express');
var cors = require('cors');

const port = (process.env.PORT || 8080);

const mongo = require('mongodb');
var MongoClient = mongo.MongoClient;

var bodyParser = require('body-parser');



var app = function () {
  const app = express();
  const indexPath  = path.join(__dirname, './public/index.html');
  const publicPath = express.static(path.join(__dirname, './public'));

  app.use( bodyParser.json() );       // to support JSON-encoded bodies
  app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
  })); 

  var user = process.env.USERNAME || '';
  var password = process.env.PASSWORD || '';
  var connectionString = user ? 'mongodb://' + user + ':' + password + '@ds021462.mlab.com:21462/frandrewdb' : 'mongodb://localhost:27017/franDB';


  app.use(cors({origin: 'http://localhost:3000'}));
  app.use('/', publicPath);
  app.get('/', function (_, res) { res.sendFile(indexPath); });

  app.get('/db/keys/:password', function(req, res){
    var db;

    MongoClient.connect(connectionString, (err, database) => {
      database
      .collection('accessKeys')
      .find({'password': req.params.password})
      .toArray(function(err,results){
        res.send(results);
        database.close();
      });
    });


  });

  return app;
}();



app.listen(port);
console.log(`Listening at http://localhost:${port}`);