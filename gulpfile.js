var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var gutil = require("gulp-util");
var sourcemaps = require('gulp-sourcemaps');
var webpackConfig = require("./webpack.config.js");
var webpackBuildConfig = require("./webpack.build.config.js");
var stream = require('webpack-stream');
var webpack = require("webpack");




gulp.task('build', function(){
  return gulp.src(['app/**/*.js'])
      // .pipe(sourcemaps.init())
      .pipe(stream(webpackBuildConfig))
      // .pipe(sourcemaps.write())
      .pipe(gulp.dest('build/'));
});

gulp.task('copy-html', function(){    
    gulp.src(['index.html'])
        .pipe(gulp.dest('packaged/'));
  });

gulp.task('copy-style', function(){    
    gulp.src(['style/**/*'])
        .pipe(gulp.dest('packaged/style'));
  });

gulp.task('copy-built', ['build'], function(){    
    gulp.src(['build/*'])
        .pipe(gulp.dest('packaged/build'));
  });

gulp.task('webpack', ['copy-html', 'copy-style', 'copy-built', ]);
