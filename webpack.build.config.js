var path = require('path');
var webpack = require('webpack');
const merge = require('webpack-merge');


const PATHS = {
 app:   path.join(__dirname, 'app'              ),
 public: path.join(__dirname, 'public'            ),
 entry: path.join(__dirname, 'app', 'index.jsx' ),
};


module.exports = {
  devtool: 'source-map',
  entry: [
    PATHS.entry
  ],
  resolve: {
    //allows to refer to file.jsx as file (without the extension)
    extensions: ['', '.js', '.jsx']
  },
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'bundle.js',
    publicPath: '/'
  },
  plugins: [
    new webpack.DefinePlugin({
        DEVELOPMENT: false,
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        }
    }),
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      compress: {
        warnings: false
      },
    }),
    new webpack.optimize.DedupePlugin()
  ],
  module: {
    loaders: [
      {
        test: /\.jsx$|\.js$/,
        loaders: ['react-hot', 'babel'],
        include: PATHS.app
      },
      {
        test: /\.css$/,
        loader: "style!css",
        include: PATHS.app
      },
      {
        test: /\.scss$/,
        loader: "style!css!sass",
        include: PATHS.app
      },
      {
        test   : /\.(ttf|eot|svg|otf|woff(2)?)(\?[a-z0-9]+)?$/,
        loader : 'file-loader'
      },
      {
        test   : /\.(jpg|png|gif)$/i,
        loaders:  [
                    'file?hash=sha512&digest=hex&name=[hash].[ext]',
                    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false' 
                  ],
      },
      {
        test  : /\.docx$/,
        loader: 'binary-loader'
      },
      {
        test  : /\.html$/,
        loader: 'html-loader',
      },
    ],
  },


};
