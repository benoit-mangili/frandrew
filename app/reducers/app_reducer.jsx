
import {List, Map, fromJS}  from 'immutable';


const INITIAL_STATE = fromJS({
});

let update_mode = (state, mode) => {
  return state.setIn(['mode'], mode);
};

let update_error = (state, error) => {
  return state.setIn(['error'], error);
};

let update_user_message = (state, user_msg) => {
  return state.setIn(['user_msg'], user_msg);
};

let reset = (state) => {
  return state.setIn([], INITIAL_STATE);
};

export default function reducer(state = INITIAL_STATE, action){
  let _state;
  switch(action.type) {
    case 'UPDATE_MODE':
      _state = update_mode(state, action.mode);
      return _state;
    case 'SET_ERROR':
      _state = update_error(state, action.error);
      return _state;
    case 'CLEAR_ERROR':
      _state = update_error(state, '');
      return _state;


    case 'SET_USER_MSG':
      _state = update_user_message(state, action.user_msg);
      return _state;
    case 'CLEAR_USER_MSG':
      _state = update_user_message(state, '');
      return _state;



    case 'RESET':
      _state = reset(state);
      return _state;
  }
  return state;
}