import React          from 'react';
import { connect }        from 'react-redux';
import { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';

import * as actionCreators from '../action_creators';

import { actions as notifActions, Notifs } from 're-notif';
const { notifSend, notifClear } = notifActions;

import { DefaultRoute, Link, Route, RouteHandler } from 'react-router';

import {Commoner}                from './commoner';


import * as constants from '../constants';

import './app.scss';

// import background from "./background.jpg";

export default class _App extends React.Component {

  getMenuItem(menuItemTo, menuItemLabel){
    return <li key={menuItemTo + "_key"}><Link to={menuItemTo}><div className='menuItem'>{menuItemLabel}</div></Link></li>;
  }
  
  tryPassword() {
    this.props.actions.enterPassword(this.refs.password.value);
  }
  logout() {
    this.props.actions.logout();
  }

  render() {
    let content;
    if(this.props.mode === 'commoner') {
        content = <div>
                    <div className="menu">
                      <a href='#'  onClick={ this.logout.bind(this) }>Logout</a>
                    </div>
                    <Commoner/>
                  </div>;
    }
    else if(this.props.mode === 'peasant') {
        content = <div>peasant
                    <div className="menu">
                      <a href='#'  onClick={ this.logout.bind(this) }>Logout</a>
                    </div>
                  </div>;
    } else {
        content = <div className="password">
                    <div>
                      Enter your password:
                    </div>
                    <input type='password' ref='password'></input>  
                    <a href='#' onClick={ this.tryPassword.bind(this) }>enter</a>
                    <div>
                      {this.props.user_msg}
                    </div>    
                  </div>;
    }


    return  <div className="mainDiv">
              <div>
                {content}
              </div>    
            </div>;
  }
}





const mapStateToProps = (state) => {
    return {
      mode:   state.getIn(['app','mode']),
      user_msg: state.getIn(['app','user_msg']),
    };
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch),
  };
};

export const App = connect(
  mapStateToProps 
 ,mapDispatchToProps
)(_App);
