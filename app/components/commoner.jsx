import React          from 'react';
import { Component, PropTypes } from 'react';

import * as constants from '../constants';

import './commoner.scss';

import IMG_2363amendedbw from "./IMG_2363amendedbw.jpg";
import Newimage1bw from "./New image 1 bw.jpg";

export const Commoner = class Commoner extends React.Component {

  render() {
    return  <div className="commoner">
              <div className="title">
                A title
              </div>    
              <div className="subtitle">
                and maybe a subtitle
              </div>    
              <div className="mainText">
                <p>
                  We would love to share our “Together Forever” Day on
Thursday 29th December 2016.
Drinks reception from 2.30pm followed by our Ceremony at 3:00pm
in the Grand Hall at Letchworth Hall Hotel, Letchworth, Herts, SG6 3NP
After please join us for dinner, then a Part-aa-yy AT 6:00pm until Midnight
All info can be found at www.frandrew.co.uk
We have reserved some bedrooms with special rates.
Please call the hotel directly on 01462 683747
quoting FrAndrews wedding Ref 66601
                </p>
                <img src={IMG_2363amendedbw} />
                <p>Integer volutpat, nunc non porttitor accumsan, sem est euismod ligula, vitae ullamcorper purus diam et tellus. Duis eleifend est eu lorem facilisis, quis tempus tortor sollicitudin. Morbi sed faucibus tellus, in rhoncus quam. Ut rutrum sapien urna, nec condimentum purus lacinia at. Sed nec erat ultrices, rhoncus mauris id, porta sapien. Sed condimentum pulvinar libero, vel laoreet ex eleifend sed. Aenean ac lacus quis enim rutrum lacinia. Aliquam magna dui, vehicula non consequat vel, bibendum eu erat.</p>
                <img src={Newimage1bw} />
                <p>Donec dignissim, ipsum vel lacinia semper, nunc nisi ultrices erat, dignissim pretium ex lorem porttitor libero. Nullam luctus sit amet ligula eget cursus. Etiam vel pellentesque dolor. Quisque id semper erat. Suspendisse at enim id diam suscipit scelerisque. Proin ac ligula eu tellus congue pellentesque. Pellentesque aliquet nec odio vitae ullamcorper.</p>
                <p>Pellentesque fermentum lectus in ipsum finibus dictum. Quisque vitae est non augue accumsan tincidunt. Duis a mauris libero. Praesent pretium ante ac eros condimentum, tincidunt mattis felis vestibulum. Pellentesque et massa vitae neque venenatis laoreet nec ut orci. Nulla facilisi. Nulla facilisi. Aliquam ornare eget ligula vitae mollis. Aenean nec semper mi. Vivamus malesuada nibh dolor, non malesuada tortor laoreet at. Maecenas tincidunt sem et nulla aliquam mollis. Quisque ut neque commodo, fermentum mauris non, egestas diam. Aenean id magna in orci ullamcorper blandit sed ac felis. Duis porttitor nisi sed placerat rutrum. Sed ac lacus id urna tristique consequat. Praesent malesuada et urna nec lacinia.</p>
                <p>Aenean tincidunt odio a sapien pharetra, a vehicula justo dignissim. Maecenas commodo libero lacus, nec sodales ex dapibus at. Aenean molestie ac enim in rhoncus. Suspendisse eu neque sollicitudin, semper urna nec, gravida nisi. Etiam at urna eget eros placerat porttitor. Quisque quis sodales libero. Aenean dignissim scelerisque ante quis blandit. Pellentesque sit amet ante tellus. Quisque sodales in neque quis mattis. Sed finibus blandit leo, sed feugiat tellus. Nullam sollicitudin, dui vel faucibus vehicula, sem ipsum vulputate nisi, a aliquet dolor nibh vitae dolor. Fusce consectetur egestas venenatis. Phasellus non ex metus.</p>
              </div>    
            </div>;
  }
};

