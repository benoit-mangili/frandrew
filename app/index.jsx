import React                          from 'react';
import ReactDOM                       from 'react-dom';
import { Router, hashHistory, Route } from 'react-router';

// middleware and redux
import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware                           from 'redux-thunk';
import { Provider }                              from 'react-redux';
import { combineReducers }                       from 'redux-immutable';

// reducers
import { reducer as notifReducer } from 're-notif';
import appReducer           from './reducers/app_reducer';

// utilities...
import $ from 'jquery';
import _ from 'lodash';
window.jQuery = $;
window._ = _;
import * as constants from './constants';


// For the routes...
import { App }                from './components/app';

import { List, Map, fromJS } from 'immutable';

import './fonts.scss';

const routes =  <Route   name='app'           path='/'                  component={App}> 
                </Route>;


const initialState = fromJS({
  "app": { 
    "mode": "",
    'error': '',
    'user_msg': '',
  }
});


let enhancer;

if (DEVELOPMENT) {
  enhancer = compose(
    applyMiddleware(thunkMiddleware),
    window.devToolsExtension ? window.devToolsExtension() : undefined
  );
} else {
  enhancer = applyMiddleware(thunkMiddleware);
}

const store = createStore(
  combineReducers({
    app:appReducer, 
  }),
  initialState, 
  enhancer
);


ReactDOM.render(
  <div className="Top">
    <Provider store={store} >
      <Router history={hashHistory}>{routes}</Router>
    </Provider>
  </div>,
  document.getElementById('app'));
