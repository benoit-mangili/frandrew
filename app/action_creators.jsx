import * as constants     from './constants';
import $ from 'jquery';

import { actions as notifActions, Notifs } from 're-notif';
const  { notifSend, notifClear } = notifActions;


export function enterPassword(password){
  return function(dispatch, getState) {

    let url = "/db/keys";
    if (DEVELOPMENT) {
      url = "http://localhost:8080" + url;
    }
    $.ajax({
      url: url + '/' + password,
      method: 'GET',
    })
    .then((results) => {
      console.log(results);
      if (results.length > 0){
        dispatch(setAppMode(results[0].mode));
        dispatch(clearUserMessage());
      } else {
        dispatch(logout());
        dispatch(setUserMessage('Password not valid.'));

      }
    });
  };
}

export function setAppMode(mode){
  return {
    type: 'UPDATE_MODE',
    mode: mode,
  };
}

export function setError(error){
  return {
    type: 'SET_ERROR',
    error,
  };
}

export function clearError(){
  return {
    type: 'CLEAR_ERROR',
  };
}

export function setUserMessage(user_msg){
  return {
    type: 'SET_USER_MSG',
    user_msg,
  };
}

export function clearUserMessage(){
  return {
    type: 'CLEAR_USER_MSG',
  };
}



export function logout(){
  return {
    type: 'UPDATE_MODE',
    mode: '',
  };
}


// // Start the calculation of the predictions.
// export function startAnalysis(){
//   return function(dispatch, getState) {

//     let p = new Promise( (resolve, reject) => { resolve(getState().toJS().dataModel); } );

//     p.then((configuration)=>{
//       return Calculator.calculate(configuration);
//     })
//     .then((configuration)=>{
//       // if all went well, we set the current stage etc.
//       let firstStage = _.chain(configuration.stages).map((s)=> s ).sortBy('order').map((s)=>s.name).value()[0];
//       let firstAttribute = _.keys( configuration.stages[firstStage].equations)[0];
//       dispatch(setCurrentStage(firstStage));
//       dispatch(setCurrentAttribute(firstAttribute));
//       return configuration;
//     })
//     .then((configuration)=>{
//       // and update the data model to contain the results of the calculation
//       dispatch(updateDataModel(configuration));
//       // as well as updating the state of the app
//       dispatch(updateAppState({status:constants.CALCULATION_SUCCEDED}));
//       dispatch(notifSend({"message": "The configuration has been loaded and analysed successfully", kind: 'success', dismissAfter: 2000}));
//     })
//     .catch((error)=>{
//       dispatch(notifSend({"message": "Something wrong happened. Impossible to load the configuration: "+error, kind: 'danger', dismissAfter: 2000}));
//       console.error("error ", error);
//     });
//   };
// }

// // Load the configuration file (Containing the definitions)
// export function loadConfiguration(rawData, settings){
//   return function(dispatch, getState) {

//     let p = new Promise( (resolve, reject) => { resolve(getState().toJS().dataModel); } );

//     p.then((configuration) => {
//       return DesignExpertLoader.loadConfiguration(rawData, configuration);
//     })
//     .then((configuration)=>{
//       dispatch(updateDataModel(configuration));
//       dispatch(updateAppState({status:constants.PARTIALLY_LOADED}));
//       dispatch(evaluate(configuration)); // must be after the updateAppState!
//       dispatch(notifSend({"message": "The configuration has been loaded successfully", kind: 'success', dismissAfter: 2000}));
//     })
//     .catch((error)=>{
//       dispatch(notifSend({"message": "Something wrong happened. Impossible to load the configuration: "+error, kind: 'danger', dismissAfter: 2000}));
//       console.error("error ", error);
//     });
//   };
// }

// export function loadDataModel(rawModels, settings){
//   return function(dispatch, getState) {

//     let p = new Promise( (resolve, reject) => { resolve(getState().toJS().dataModel); } );
//     p
//     .then((existingDataModel) => {
//       dispatch(updateAppState({errors:[]}));
//       return DesignExpertLoader.loadModels(rawModels, existingDataModel);
//     })
//     .then((configuration) => {
//       dispatch(updateDataModel(configuration));
//       dispatch(updateAppState({status:constants.PARTIALLY_LOADED}));
//       dispatch(evaluate(configuration)); // must be after the updateAppState!
//       dispatch(notifSend({"message": "The configuration has been loaded successfully", kind: 'success', dismissAfter: 2000}));
//     })
//     .catch((errors) => {
//       dispatch(updateAppState({errors:errors}));
//       dispatch(notifSend({"message": "Something wrong happened. Impossible to load the configuration: "+errors, kind: 'danger', dismissAfter: 2000}));
//       console.error("error ", errors);
//     });
//   };
// }

// export function resetDataModel(){
//   return function(dispatch, getState) {

//     let p = new Promise( (resolve, reject) => { resolve(); } );
//     p
//     .then(()=>{
//       dispatch( cleanDataModel() );
//       dispatch( updateAppState({status:constants.BLANK}) );
//       dispatch( notifSend({"message": "Reset successful", kind: 'success', dismissAfter: 2000}));
//     })
//     .catch((errors)=>{
//       dispatch(updateAppState({errors:errors}));
//       dispatch(notifSend({"message": "Something wrong happened. Impossible to load the configuration: "+errors, kind: 'danger', dismissAfter: 2000}));
//       console.error("error ", errors);
//     });
//   };
// }

// export function updateDataModel(newDataModel){
//   return {
//     type: 'REPLACE_DATAMODEL',
//     data: newDataModel
//   };
// }

// export function evaluate(dataModel){
//   return function(dispatch, getState) {
//     let evaluation = DesignExpertLoader.evaluate(dataModel, getState().toJS().app.settings);
//     if (evaluation.problemsFound.length === 0 ){
//       dispatch(updateAppState({status:constants.FULLY_LOADED}));
//     }
//     dispatch(notifSend({"message": "Evaluation completed", kind: 'info', dismissAfter: 2000}));
//     dispatch(updateEvaluation(evaluation));
//   };
// }

// export function reevaluate(dataModel){
//   return function(dispatch, getState) {
//     dispatch(evaluate(getState().toJS().dataModel));
//   };
// }

// export function updateEvaluation(evaluation){
//   return {
//     type: 'UPDATE_EVALUATION',
//     evaluation
//   };
// }

// export function cleanDataModel(){
//   return {
//     type: 'RESET_DATAMODEL',
//   };
// }

// export function setCurrentStage(stageName){
//   return {
//     type: 'SET_CURRENTSTAGE',
//     stageName
//   };
// }

// export function setCurrentAttribute(attributeName){
//   return {
//     type: 'SET_CURRENTATTRIBUTE',
//     attributeName
//   };
// }

// export function updateExpertMode(expertMode){
//   return {
//     type: 'UPDATE_EXPERTMODE',
//     expertMode
//   };
// }

// export function updateSettings(newSettings){
//   return {
//     type: 'UPDATE_SETTINGS',
//     newSettings
//   };
// }

// export function updateAppState(newAppState){
//   return {
//     type: 'UPDATE_APPSTATE',
//     newAppState
//   };
// }

// export function setNominalValue(stageName, nominalValues){
//   return function(dispatch, getState) {
//     let p = new Promise( (resolve, reject) => { resolve(getState().toJS().dataModel); } );
//     p
//     .then((configuration)=>{
//       return Core.setInNominalValue(configuration, stageName, nominalValues);
//     })
//     .then((configuration)=>{
//       return Calculator.calculate(configuration);
//     })
//     .then((configuration)=>{
//       dispatch(updateDataModel(configuration));
//       dispatch(notifSend({"message": "The model has been updated successfully", kind: 'success', dismissAfter: 2000}));
//     })
//     .catch((error)=>{
//       dispatch(notifSend({"message": "Something wrong happened. Impossible to update the model: "+error, kind: 'danger', dismissAfter: 2000}));
//       console.error("error ", error);
//     });
//   };
// }

// export function setBetaValue(stageName, attributeName,coefficient, newBetaValue){
//   return function(dispatch, getState) {
//     let p = new Promise( (resolve, reject) => { resolve(getState().toJS().dataModel); } );
//     p
//     .then((configuration)=>{
//       return Core.setBeta(configuration, stageName, attributeName,coefficient, newBetaValue);
//     })
//     .then((configuration)=>{
//       return Calculator.calculate(configuration);
//     })
//     .then((configuration)=>{
//       dispatch(updateDataModel(configuration));
//       dispatch(notifSend({"message": "The model has been updated successfully", kind: 'success', dismissAfter: 2000}));
//     })
//     .catch((error)=>{
//       dispatch(notifSend({"message": "Something wrong happened. Impossible to update the model: "+error, kind: 'danger', dismissAfter: 2000}));
//       console.error("error ", error);
//     });
//   };
// }

// export function setIPLValue(stageName, iplKey, newValue){
//   return function(dispatch, getState) {
//     let p = new Promise( (resolve, reject) => { resolve(getState().toJS().dataModel); } );
//     p
//     .then((configuration)=>{
//       return Core.setIPLValue(configuration, stageName, iplKey, newValue);
//     })
//     .then((configuration)=>{
//       return Calculator.calculate(configuration);
//     })
//     .then((configuration)=>{
//       dispatch(updateDataModel(configuration));
//       dispatch(notifSend({"message": "The model has been updated successfully", kind: 'success', dismissAfter: 2000}));
//     })
//     .catch((error)=>{
//       dispatch(notifSend({"message": "Something wrong happened. Impossible to update the model: "+error, kind: 'danger', dismissAfter: 2000}));
//       console.error("error ", error);
//     });
//   };
// }

// export function setIPLOp(stageName, iplKey, newOp){
//   return function(dispatch, getState) {
//     let p = new Promise( (resolve, reject) => { resolve(getState().toJS().dataModel); } );
//     p
//     .then((configuration)=>{
//       return Core.setIPLOp(configuration, stageName, iplKey, newOp);
//     })
//     .then((configuration)=>{
//       return Calculator.calculate(configuration);
//     })
//     .then((configuration)=>{
//       dispatch(updateDataModel(configuration));
//       dispatch(notifSend({"message": "The model has been updated successfully", kind: 'success', dismissAfter: 2000}));
//     })
//     .catch((error)=>{
//       dispatch(notifSend({"message": "Something wrong happened. Impossible to update the model: "+error, kind: 'danger', dismissAfter: 2000}));
//       console.error("error ", error);
//     });
//   };
// }