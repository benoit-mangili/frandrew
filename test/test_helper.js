import chai from 'chai';
import chaiImmutable from 'chai-immutable';

chai.use(chaiImmutable);
chai.should();

import $ from 'jquery';
import _ from 'lodash';

global.$ = $;
global._ = _;
